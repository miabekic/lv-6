﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_2
{
    interface IAbstractIterator
    {
        Product First();
        Product  Next();
        bool IsDone { get; }
        Product Current { get; }
    }
}
