﻿using System;
using System.Collections.Generic;

namespace ZADATAK_2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = new List<Product>() {  new Product("Mobilni telefon", 7500.15),
                                                            new Product("Bezicne slusalice", 500.26),
                                                            new Product("Bezicni punjac", 850.14) };
            Box box = new Box(products);
            IAbstractIterator iterator = box.GetIterator();
            for (int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(iterator.Current+"kn");
                iterator.Next();
            }
        }
    }
}
