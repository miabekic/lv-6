﻿using System;

namespace ZADATAK_3
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDoItem item1 = new ToDoItem("Nova biljeska", "Mia", DateTime.Now.AddMonths(5));
            ToDoItem item2 = new ToDoItem("Ucenje", "8 predmeta", DateTime.Now.AddMonths(6));
            CareTaker careTaker = new CareTaker();
            Console.WriteLine("Drugi item:\n" + item2);
            careTaker.Add(item1.StoreState());
            careTaker.Add(item2.StoreState());
            item2.ChangeTask("5 predmeta");
            item2.ChangeTimeDue(DateTime.Now.AddMonths(2));
            Console.WriteLine("\nDrugi item nakom promjena:\n" + item2);
            item2.RestoreState(careTaker.Get());
            Console.WriteLine("\nDrugi item nakon povratka starih podataka:\n" + item2);
            careTaker.Remove();
            Console.WriteLine("\nProvjera brisanja: " + careTaker.Get().Title);
        }
    }
}
