﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_3
{
    class CareTaker
    {
        private Stack<Memento> savedItems = new Stack<Memento>();
        public Memento PreviousState { get; set; }
        public void Add(Memento savedItem)
        {
            savedItems.Push(savedItem);
        } 
        public Memento Get()
        {
            return savedItems.Peek();
        }
        public void Remove()
        {
            savedItems.Pop();
        }
    }
}
