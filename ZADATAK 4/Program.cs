﻿using System;

namespace ZADATAK_4
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount account = new BankAccount("Mia", "Frane Krste Frankopana 10", 10.5m);
            Memento savedAccount = account.StoreState();
            Console.WriteLine("Status: " + account);
            account.ChangeOwnerAddress("Ivana Gorana Kovacica 4");
            account.UpdateBalance(1m);
            Console.WriteLine("\nNovi status: " + account);
            account.RestoreState(savedAccount);
            Console.WriteLine("\nStatus nakon povratka podataka: " + account);
        }
    }
}
