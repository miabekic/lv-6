﻿using System;

namespace ZADATAK_6
{
    class Program
    {
        static void Main(string[] args)
        {
            string stringToCheck1 = "7mmmmiAb";
            string stringToCheck2 = "miabeki";
            StringChecker digitChecker = new StringDigitChecker();
            StringLengthChecker lengthChecker = new StringLengthChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            digitChecker.SetNext(lengthChecker);
            lengthChecker.SetNext(upperCaseChecker);
            upperCaseChecker.SetNext(lowerCaseChecker);
            Console.WriteLine(digitChecker.Check(stringToCheck1 + "\n"));
            Console.WriteLine(digitChecker.Check(stringToCheck2 + "\n"));
        }
    }
}
