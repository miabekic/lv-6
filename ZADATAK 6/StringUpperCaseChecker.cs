﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_6
{
    class StringUpperCaseChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasUpperCase = false;
            foreach(char element in stringToCheck)
            {
                if (char.IsUpper(element))
                {
                    hasUpperCase = true;
                }
            }
            return hasUpperCase;
        }
    }
}
