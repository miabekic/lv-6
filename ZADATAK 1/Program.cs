﻿using System;
using System.Collections.Generic;
using System.Resources;

namespace ZADATAK_1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Note> notes = new List<Note>() {new Note("Popis predmeta", "RPPOON, SIS"),
                                                  new Note("Nahrani zivotinje", "Lunu, mačke i pacice"),
                                                  new Note("Kucne obaveze", "Pranje vesa i suda")};
            Notebook notebook = new Notebook(notes);
            IAbstractIterator iterator = notebook.GetIterator();
            for(int i = 0; i < notebook.Count; i++)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
