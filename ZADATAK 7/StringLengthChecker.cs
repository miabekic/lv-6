﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    class StringLengthChecker:StringChecker
    {
        private int minLength;
        public StringLengthChecker()
        {
            minLength = 6;
        }
        public StringLengthChecker(int minLength)
        {
            this.minLength = minLength;
        }
        protected override bool PerformCheck(string stringToCheck)
        {
            bool isLongEnough = false;
            if (stringToCheck.Length >= minLength)
            {
                isLongEnough = true;
            }
            return isLongEnough;
        }
    }
}
