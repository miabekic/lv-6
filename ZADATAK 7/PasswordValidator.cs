﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    class PasswordValidator
    {
        private StringChecker firstChecker;
        private StringChecker lastChecker;
        public PasswordValidator(StringChecker firstChecker) {
            this.firstChecker = firstChecker;
            this.lastChecker = this.firstChecker;
        }
        public void AddChecker(StringChecker checker)
        {
            lastChecker.SetNext(checker);
            lastChecker = checker;
        }
        public bool CheckPassword(string stringToCheck)
        {
            return firstChecker.Check(stringToCheck);
        }
    }
}
