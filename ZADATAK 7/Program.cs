﻿using System;

namespace ZADATAK_7
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker lengthChecker = new StringLengthChecker(5);
            StringDigitChecker digitChecker = new StringDigitChecker();
            StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
            PasswordValidator validator = new PasswordValidator(lengthChecker);
            validator.AddChecker(digitChecker);
            validator.AddChecker(lowerCaseChecker);
            validator.AddChecker(upperCaseChecker);
            Console.WriteLine(validator.CheckPassword("148miaB"));
        }
    }
}
