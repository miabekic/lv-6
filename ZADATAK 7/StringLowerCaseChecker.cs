﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    class StringLowerCaseChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasLowerCase = false;
            foreach(char element in stringToCheck)
            {
                if (char.IsLower(element))
                {
                    hasLowerCase = true;
                }
            }
            return hasLowerCase;
        }
    }
}
