﻿


using System;
using System.Collections.Generic;
using System.Text;

namespace ZADATAK_7
{
    class StringDigitChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasDigit = false;
            foreach(char element in stringToCheck)
            {
                if (char.IsDigit(element))
                {
                    hasDigit = true;
                }
            }
            return hasDigit;
        }
    }
}
