﻿using System;

namespace ZADATAK_5
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger =
           new FileLogger(MessageType.ERROR | MessageType.WARNING| MessageType.INFO, @"C:\Users\Mia\Desktop\LV-6\ZADATAK 5\logFile.txt");
            logger.SetNextLogger(fileLogger);
            logger.Log("Doslo je do pogreske!", MessageType.ERROR);
            logger.Log("Doslo je do curenja podataka", MessageType.WARNING);
            logger.Log("Ponovno pokrenite racunalo", MessageType.INFO);
        }
    }
}
